package ro.itschool.spring.tutorial.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.itschool.spring.tutorial.model.Account;
import ro.itschool.spring.tutorial.repositories.AccountRepository;

@Service
public class AccountsService implements InitializingBean {

	@Autowired
	private AccountRepository accountRepository;
	
	/**
	 * GETS CALLED AFTER AUTOWIRE
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		// SAFE TO USE accountRepository
		this.createAccounts();
	}
	
	private void createAccounts() {
		Account account1 = new Account();
		account1.setEmail("account1@gmail.com");
//		account1.setId(1L);
		account1.setName("Account One");
		account1.setPassword("1234");
		
		Account account2 = new Account();
		account2.setEmail("account2@gmail.com");
//		account2.setId(2L);
		account2.setName("Account Twoooo");
		account2.setPassword("4321");
		
		this.accountRepository.save(account1);
		this.accountRepository.save(account2);
	}		
	
	public List<Account> getActiveAccount() {
		Iterable<Account> accountsIt = this.accountRepository.findAll();
		
		List<Account> accounts = new ArrayList<Account>();
		for (Account account : accountsIt) {
			accounts.add(account);
		}
		
		return accounts;
	}
}
