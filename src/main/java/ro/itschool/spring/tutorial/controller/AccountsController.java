package ro.itschool.spring.tutorial.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.itschool.spring.tutorial.model.Account;
import ro.itschool.spring.tutorial.service.AccountsService;

@RestController
public class AccountsController {

	@Autowired
	private AccountsService accountsService;
	
	@GetMapping(path = "accounts", produces = "application/json")
	public List<Account> getAccounts() throws Exception {
		return this.accountsService.getActiveAccount();
	}
}
